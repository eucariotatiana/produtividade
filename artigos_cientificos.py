#!/usr/bin/env python

from store_data import StoreData


class ExtrairArtigosCientificos:
    def __init__(self):
        self.artigos_cientificos_list = list()

    def extrair_dados_artigos_cientificos_csv(self):
        print('Abrindo arquivo csv...')
        try:
            file = open('artigos_cientificos.csv', encoding='utf-8')
            print('Arquivo aberto com sucesso!')
            cabecalho_artigos_cientificos = file.readline()
            lista_dados = file.readlines()
            file.close()
            if lista_dados:
                for linha in lista_dados:
                    artigos_cientificos = ArtigosCientificos()
                    raw_data = linha.split("\n")[0].split(",")
                    artigos_cientificos.id_do_artigo = raw_data[0]
                    artigos_cientificos.titulo_do_artigo = raw_data[1]
                    artigos_cientificos.autoria = raw_data[2]
                    artigos_cientificos.area_de_conhecimento = raw_data[3]
                    artigos_cientificos.numero_de_paginas_do_artigo = int(raw_data[4])
                    artigos_cientificos.impresso_digital = True if raw_data[5] == '1' else False
                    artigos_cientificos.ano_de_publicacao = int(raw_data[6])
                    self.artigos_cientificos_list.append(artigos_cientificos)
                print('Arquivo csv extraído com sucesso!')
            else:
                print('Não foram encontrados dados para ArtigosCientificos no arquivo.')
                file_write = open('artigos_cientificos.csv', 'w')
                file_write.write(cabecalho_artigos_cientificos)
                file_write.close()
        except Exception as e:
            print('Extração não realizada. Erro:{erro}'.format(erro=e))

    def extrair_dados_artigos_cientificos_banco_de_dados(self):
        with StoreData() as connection:
            artigos_cientificos = connection.select_artigos_cientificos()
            for linha in artigos_cientificos:
                artigos_cientificos = ArtigosCientificos()
                artigos_cientificos.id_do_artigo = linha[0]
                artigos_cientificos.titulo_do_artigo = linha[1]
                artigos_cientificos.autoria = linha[2]
                artigos_cientificos.area_de_conhecimento = linha[3]
                artigos_cientificos.numero_de_paginas_do_artigo = linha[4]
                artigos_cientificos.impresso_digital = linha[5]
                artigos_cientificos.ano_de_publicacao = linha[6]
                self.artigos_cientificos_list.append(artigos_cientificos)


class ArtigosCientificos:
    def __init__(self):
        self.id_do_artigo = None
        self.titulo_do_artigo = None
        self.autoria = None
        self.area_de_conhecimento = None
        self.numero_de_paginas_do_artigo = None
        self.impresso_digital = None
        self.ano_de_publicacao = None

    def save(self):
        with StoreData() as connection:
            connection.insert_artigos_cientificos(self)