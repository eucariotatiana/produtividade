#!/usr/bin/env python

from datetime import datetime, timedelta
from store_data import StoreData


class ExtrairDadosProdutividade:
    def __init__(self):
        self.dados_produtividade_list = list()

    def extrair_dados_csv(self):
        print('Abrindo arquivo csv...')
        try:
            file = open('produtividade.csv', encoding='utf-8')
            cabecalho_produtividade = file.readline()
            print('Arquivo csv aberto com sucesso!')
            lista_dados = file.readlines()
            file.close()
            if lista_dados:
                for linha in lista_dados:
                    dado_produtividade = DadoProdutividade()
                    raw_data = linha.split("\n")[0].split(",")
                    dado_produtividade.dia = datetime.strptime(raw_data[0], "%d/%m/%y")
                    dado_produtividade.tempo = timedelta(minutes=int(raw_data[1]))
                    dado_produtividade.numero_de_palavras = int(raw_data[2])
                    dado_produtividade.meta_diaria = int(raw_data[3])
                    self.dados_produtividade_list.append(dado_produtividade)
                print('Arquivo csv extraído com sucesso!')
            else:
                print('Não foram encontrados dados para produtividade no arquivo.')
            file_write = open('produtividade.csv', 'w')
            file_write.write(cabecalho_produtividade)
            file_write.close()
        except Exception as e:
            print('Extração não realizada. Erro: {erro}'.format(erro=e))

    def extrair_dados_produtividade_banco_de_dados(self):
        with StoreData() as connection:
            dados_produtividade = connection.select_dados_de_produtividade()
            for linha in dados_produtividade:
                dado_produtividade = DadoProdutividade()
                dado_produtividade.dia = linha[1]
                dado_produtividade.tempo = linha[2]
                dado_produtividade.numero_de_palavras = linha[3]
                dado_produtividade.meta_diaria = linha[4]
                self.dados_produtividade_list.append(dado_produtividade)

    def extrair_dados_produtividade_banco_de_dados_mais_de_cinquenta(self):
        with StoreData() as connection:
            dados_produtividade = connection.select_dados_de_produtividade_mais_de_cinquenta()
            for linha in dados_produtividade:
                dado_produtividade = DadoProdutividade()
                dado_produtividade.dia = linha[1]
                dado_produtividade.tempo = linha[2]
                dado_produtividade.numero_de_palavras = linha[3]
                dado_produtividade.meta_diaria = linha[4]
                self.dados_produtividade_list.append(dado_produtividade)

    def conta_dados_produtividade_meta_diaria(self):
        with StoreData() as connection:
            return connection.conta_dados_de_produtividade_meta_diaria()


class DadoProdutividade:
    def __init__(self):
        self.dia = None
        self.tempo = None
        self.numero_de_palavras = None
        self.meta_diaria = None

    def save(self):
        with StoreData() as connection:
            print('Salvando dados...')
            connection.insert_dados_produtividade(self)
            print('Dados salvos com sucesso.')

    def print(self):
        print('Dia: {dia}\n'
              'Tempo: {tempo}\n'
              ' Palavras: {palavras}\n'
              ' Meta: {meta}'.format(
            dia=self.dia,
            tempo=self.tempo,
            palavras=self.numero_de_palavras,
            meta=self.meta_diaria))
