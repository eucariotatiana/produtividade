#!/usr/bin/env python

from store_data import StoreData


class ExtractArticleGrades:
    def __init__(self):
        self.article_grades_list = list()

    def extract_article_grades_csv(self):
        print('Abrindo arquivo csv...')
        try:
            file = open('article_grades.csv')
            print('Arquivo aberto com sucesso!')
            header_article_grades = file.readline()
            lista_dados = file.readlines()
            file.close()
            if lista_dados:
                for line in lista_dados:
                    article_grades = ArticleGrades()
                    raw_data = line.split("/n")[0].split(",")
                    article_grades.id = (raw_data[0])
                    article_grades.grades = int(raw_data[1])
                    self.article_grades_list.append(article_grades)
                print('Arqui9vo csv extraído com sucesso!')
            else:
                print('Não foi encontrado dados para ArticleGrades no arquivo.')
            file_write = open('article_grades.csv', 'w')
            file_write.write(header_article_grades)
            file_write.close()
        except Exception as e:
            print('Extração não realizada. Erro: {erro}'.format(erro=e))


    def extract_article_grades_data_base(self):
        with StoreData() as connection:
            article_grades = connection.select_article_grades()
            for line in article_grades:
                article_grades = ArticleGrades()
                article_grades.id = line[0]
                article_grades.grades = line[1]
                self.article_grades_list.append(article_grades)


    def get_article_grades(self, raw_article_grades):
        if raw_article_grades != '':
            return int(raw_article_grades)
        return None


class ArticleGrades:
    def __init__(self):
        self.id = None
        self.article_id = None
        self.article_grades = None

    def save(self):
        with StoreData() as connection:
            connection.insert_article_grades(self)
