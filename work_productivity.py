#!/usr/bin/env python

from datetime import timedelta, datetime
from store_data import StoreData

class ExtractWorkProductivity:
    def __init__(self):
        self.work_productivity_list = list()

    def extract_work_productivity_csv(self):
        try:
            file = open('work_productivity.csv', encoding='utf-8')
            print('Abrindo arquivo csv...')
            header_work_productivity = file.readline()
            print('Arquivo aberto com sucesso!')
            lista_dados = file.readlines()
            file.close()
            if lista_dados:
                for line in lista_dados:
                    work_productivity = WorkProductivity()
                    raw_data = line.split("\n")[0].split(",")
                    work_productivity.day = datetime.strptime(raw_data[0],"%d/%m/%y")
                    work_productivity.focus_time = timedelta(minutes=int(raw_data[1]))
                    work_productivity.theory_practice = bool(raw_data[2])
                    work_productivity.knowledge_area = raw_data[3]
                    self.work_productivity_list.append(work_productivity)
                print('Arquivo csv extraído com sucesso!')
            else:
                print('Não foram encontrados dados para WorkProductivity no arquivo.')
                file_write = open('work_productivity.csv', 'w')
                file_write.write(header_work_productivity)
                file_write.close()
        except Exception as e:
            print('Extração nãp realizada. Erro:{erro}'.format(erro=e))

    def extract_work_productivity_store_data(self):
        with StoreData() as connection:
            work_productivity = connection.select_work_productivity()
            for line in work_productivity:
                work_productivity = WorkProductivity()
                work_productivity.day = line[0]
                work_productivity.focus_time = line[1]
                work_productivity.theory_practice = line[2]
                work_productivity.knowledge_area = line[3]
                self.work_productivity_list.append(work_productivity)


class WorkProductivity:
    def __init__(self):
        self.day = None
        self.focus_time = None
        self.theory_practice = None
        self.knowledge_area = None

    def save(self):
        with StoreData() as connection:
            connection.insert_work_productivity(self)


