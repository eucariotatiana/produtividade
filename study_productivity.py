#!/usr/bin/env python

from datetime import timedelta, datetime
from store_data import StoreData


class ExtractStudyProductivity:
    def __init__(self):
        self.study_productivity_list = list()

    def extract_study_productivity_csv(self):
        try:
            file = open('study_productivity.csv', encoding='utf-8')
            print('Abrindo arquivo csv...')
            header_study_productivity = file.readline()
            print('Arquivo csv aberto com sucesso!')
            lista_dados = file.readlines()
            file.close()
            if lista_dados:
                for line in lista_dados:
                    study_productivity = StudyProductivity()
                    raw_data = line.split("\n")[0].split(",")
                    study_productivity.knowledge_area = raw_data[0]
                    study_productivity.day = datetime.strptime(raw_data[1], "%d/%m/%y")
                    study_productivity.kind_of_study = raw_data[2]
                    study_productivity.time_of_study = timedelta(minutes=int(raw_data[3]))
                    study_productivity.course = True if raw_data[4] == '1' else False
                    study_productivity.finished = True if raw_data[5] == '1' else False
                    study_productivity.course_diploma = True if raw_data[6] == '1' else False
                    self.study_productivity_list.append(study_productivity)
                print('Arquivo csv extraído com sucesso!')
            else:
                print('Não foram encontrados dados para StudyProductivity')
                file_write = open('study_productivity.csv', 'w')
                file_write.write(header_study_productivity)
                file_write.close()
        except Exception as e:
            print('Extração não realizada. Erro:{erro}'.format(erro=e))

    def extract_work_productivity_data_base(self):
        with StoreData() as connection:
            study_productivity = connection.select_study_productivity()
            for line in study_productivity:
                study_productivity = StudyProductivity()
                study_productivity.Knowledge_area = line[0]
                study_productivity.day = line[1]
                study_productivity.kind_of_study = line[2]
                study_productivity.time_of_study = line[3]
                study_productivity.course = line[4]
                study_productivity.finished = line[5]
                study_productivity.course_diploma = line[6]
                self.study_productivity_list.append(study_productivity)


class StudyProductivity:
    def __init__(self):
        self.knowledge_area = None
        self.day = None
        self.kind_of_study = None
        self.time_of_study = None
        self.course = None
        self.finished = None
        self.course_diploma = None

    def save(self):
        with StoreData() as connection:
            connection.insert_study_productivity(self)


